#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : view_interface.py
# @Author : Heimann, Jannik
# @Date   : 3/15/22
# @Desc   : Schnittstelle fuer Controller der View (GUI) Klasse

class ViewInterface:
    def start_log_in_window(self):
        """
        Erstellt die GUI zum Einloggen des Nutzers.
        Der Nutzer kann durch Interaktion mit den zur Verfügung gestellten Buttons
        Events hervorrufen, welche dann Methoden im Controller aufrufen

        !Methode enthaelt mainloop!
        :return None:
        """
        pass

    def start_voting_window(self, is_noble):
        """
        Erstellt die GUI zum Voten von Musikwünschen
        Der Nutzer kann durch Interaktion mit den zur Verfügung gestellten Buttons
        Events hervorrufen, welche dann Methoden im Controller aufrufen

        !Methode enthaelt mainloop!
        :param boolean is_noble: Hat Nutzer Adelstitel (Voterechte)
        :return None:
        """
        pass

    def start_registering_window(self, is_noble=False, has_password=False):
        """
        Erstellt die GUI zum Eingeben fehlender Nutzerdaten

        :param bool is_noble: Exsistiert der Titel in der Datenbank?
        :param bool has_password: Existiert das Passwort in der Datenbank?
        :return None:
        """

    def start_password_reset_window(self, security_question):
        """
        Erstellt die GUI zum Zuruecksetzen des Passworts.
        Der Nuter muss die Sicherheitsfrage beantworten und ein neues Passwort eingeben.
        Ruft nach abschluss eine entsprechende Methode im Controller aus

        :param str security_question: Sicherheitsfrage
        :return None:
        """