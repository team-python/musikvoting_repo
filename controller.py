#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : controller.py
# @Author : Billerbeck, Ulf
# @Date   : 3/16/22
# @Desc   : Controller implementing the ContollerInterface

from song import Song
from user import User
from controller_interface import ControllerInterface
from view_interface import ViewInterface
from dbconnection import DBConnection
from model import model
import encryption


class Controller(ControllerInterface):

    __songlist = []
    __user = None
    __view = None
    __alphabetical_sorted = True
    __db = None

    def __init__(self):
        """
        Initiiert die Datenbankverbindung
        """
        self.__db = DBConnection("localhost", 3306, "JukeboxAdmin", "DatenbankLF9!", "Jukebox")
        self.sort_list()

    def create_user(self, name):
        """
        Erstellt User

        :param str name: Nutzername
        :return None:
        """
        self.__user = User(name)
        login_check = self.__db.db_select(model.login_check(name))
        if len(login_check) != 0:
            raise Exception("User already in database")
        self.__db.db_insert(model.new_user(self.__user.get_name()))

        self.__view.start_registering_window()

    def login(self, name, password):
        """
        Erstellt einen neuen Nutzer in der Datenbank
        Gibt eine Exception zurueck, wenn der Nutzer nicht eingefuegt werden kann.
        Gibt eine Exception zurueck, wenn der Nutzer bereits vorhanden ist.
        Gibt eine Exception zurueck, wenn das Passwort falsch ist.

        :param str name: Nutzername
        :param password: Passwort
        :return None:
        """
        login_check = self.__db.db_select(model.login_check(name))
        if len(login_check) == 0:
            raise Exception("User not in database")
        else:
            user_salt = self.__db.db_select(model.get_salt(name))
            if user_salt[0][0] is None:
                self.__view.start_registering_window()
            if not self.__db.db_select(model.get_password_hash(name)) == encryption.hash(user_salt + password):
                raise Exception("Wrong password!")
            else:
                self.__user = User(login_check[0][0], login_check[0][1])

                if self.__user.get_title() is None:
                    self.__view.start_registering_window()
                else:
                    if self.__user.is_noble():
                        self.__view.start_voting_window(True)
                    else:
                        self.__view.start_voting_window(False)

    def logout(self):
        """
        Logt den aktuellen Nutzer aus.

        :return None:
        """
        self.__user = None
        self.__view.start_log_in_window()

    def password_reset_clicked(self, user):
        self.__view.start_password_reset_window(self.__get_security_question(username=user))

    def __get_security_question(self, username):
        """
        Ruft die Sicherheitsfrage des Benutzers ab.
        :param str username: benutzername
        :return str: Sicherheitsfrage
        """
        return self.__db.db_select(model.get_security_question(username))

    def change_password(self, security_question_answer, new_password):
        """
        Mit der richtigen Sicherheitsfrage wird das Passwort geaendert.

        :param security_question_answer: Antwort der Sicherheitsfrage
        :param new_password: Neues Passwort
        :return:
        """
        login_check = self.__db.db_select(model.login_check(self.__user.get_name()))
        if len(login_check) == 0:
            raise Exception("User not in database")
        else:
            security_question_salt = self.__db.db_select(model.get_salt(self.__user.get_name()))
            if not self.__db.db_select(model.get_password_hash(self.__user.get_name())) == encryption.hash(security_question_salt + security_question_answer):
                raise Exception("Wrong Answer!")
            else:
                self.__db.db_insert(model.set_password_hash(benutzername=self.__user.get_name(), password_hash=encryption.hash(security_question_salt + new_password)))

    def current_user(self):
        """
        Gibt den aktuellen Nutzer zurueck.

        :return User: Benutzer classenobjekt oder None, wenn kein Nutzer eingeloged ist.
        """
        return self.__user

    def add_song(self, song):
        """
        Fuegt ein Lied in die Liederliste ein, wenn ein Benutzer angemeldet ist.
        Gibt eine Exception zurueck, falls kein Nutzer angemeldet ist.
        Gibt eine Exception zurueck, wenn das Lied bereits vorhanden ist.

        :param Song song: Einzufuegendes Lied
        :return None:
        """
        if self.__user is None:
            raise Exception("User not logged in")
        elif song.get_bandname() == '':
            raise Exception("Es wurde kein Bandname eingegeben")
        elif song.get_titelname() == '':
            raise Exception("Es wurde kein Titelname eingegeben")
        elif song.get_genre() == '':
            raise Exception("Es wurde kein Genre eingegeben")
        else:
            self.__db.db_insert(model.new_song(song.get_titelname(), song.get_bandname(), song.get_genre()))
            self.sort_list()

    def add_vote(self, song):
        """
        Fuegt dem Nutzer eine Stimme für den entsprechenden Song hinzu. Bis zu fünf Stimmen
        koennen abgegeben werden.
        Gibt eine Exception zurueck, falls kein Nutzer angemeldet ist.

        :param Song song:
        :return None:
        """
        if self.__user is None:
            raise Exception("User not logged in")
        if not self.__user.is_noble():
            raise Exception("User is not noble, can not vote")
        tmp = self.__db.db_select(model.countVotesByUser(self.__user.get_name()))[0]
        votesByUser = 0
        for rows in tmp:
            votesByUser = rows
        if votesByUser > 4:
            self.__db.db_insert(model.deleteFirstVote(self.__user.get_name()))
        self.__db.db_insert(model.vote(song.get_id(), self.__user.get_name()))
        self.sort_list()

    def get_list(self):
        """
        Gibt eine Liste von Tuplen (Lied, Stimmern) zurueck

        :return [(str,int)]: Liederliste
        """
        return self.__songlist

    def sort_alphabetical(self):
        """
        Sortiert die Liste alphabetisch

        :return None:
        """
        self.__alphabetical_sorted = True
        self.sort_list()

    def sort_votes(self):
        """
        Sortiert die Liste nach der Anzahl der Votes.

        :return None:
        """
        self.__alphabetical_sorted = False
        self.sort_list()

    def sort_list(self):
        """
        Sortiert die Liste entsprechend der Ordnung

        :return None:
        """
        sorted_list = None
        if self.__alphabetical_sorted:
            sorted_list = self.__db.db_select(model.select_musikwuensche_alph())
        else:
            sorted_list = self.__db.db_select(model.select_musikwuensche_votes())
        self.__songlist.clear()
        for row in sorted_list:
            self.__songlist.append(Song(row[2], row[1], row[3], song_id=row[0], song_votes=row[4]))

    def set_view(self, view):
        """
        Macht dem Controller die View bekannt.

        :param ViewInterface view:
        :return None:
        """
        self.__view = view

    def add_attributes_to_user(self, title, password, security_question, security_question_answer):
        """
        Erstellt neuen Gast in der Datenbank bzw. fuegt bestehendem Gast die Attribute hinzu.

        :param str title: Adelstitel
        :param str password: Passwort
        :param security_question: Sicherheitsfrage
        :param security_question_answer: Antwort Sicherheitsfrage
        :return:
        """

        if title == '':
            title = 'Fussvolk'

        salt = encryption.new_salt()
        self.__db.db_insert(model.set_password_hash(self.__user.get_name(), encryption.hash(salt + password)))
        self.__db.db_insert(model.set_salt(benutzername=self.__user.get_name(), salt=salt))

        self.__db.db_insert(model.update_title(self.__user.get_name(), title))
        self.__user.set_title(title)
        self.__db.db_insert(model.set_security_question(benutzername=self.__user.get_name(), question=security_question))
        self.__db.db_insert(
            model.set_answer_hash(benutzername=self.__user.get_name(), answer_hash=encryption.hash(self.__db.db_select(model.get_salt(self.__user.get_name())) + security_question_answer)))

        if self.__user.is_noble():
            self.__view.start_voting_window(True)
        else:
            self.__view.start_voting_window(False)



if __name__=="__main__":
    testcontroller = Controller()
    try:
        testcontroller.create_user("User")
        testcontroller.logout()
    except Exception as e:
        print(e)

    testcontroller.login("User", "abc")
    l = testcontroller.get_list()
    try:
        testcontroller.add_song(Song("Band1", "Titel1", "Genre1"))
    except Exception as e:
        print(e)

    testcontroller.add_vote(testcontroller.get_list()[1])
    for song in l:
        print(song.toString() + " Votes:" + str(song.get_votes()))

    testcontroller.sort_votes()
    try:
        testcontroller.add_song(Song("Band2", "Titel2", "Genre2"))
    except Exception as e:
        print(e)

    testcontroller.add_vote(testcontroller.get_list()[0])
    for song in l:
        print(song.toString() + " Votes:" + str(song.get_votes()))

