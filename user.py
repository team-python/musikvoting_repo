#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : user.py
# @Author : Billerbeck, Ulf
# @Date   : 3/16/22
# @Desc   : Klasse fuer den Benutzer


class User:
    """
    Klasse fuer den Benutzer um eine einfache Darstellung fuer die GUI zu bekommen.
    """
    __name = None
    __titel = None

    def __init__(self, name, title=None):
        """
        Konstruktor der Klasse User

        :param str name:
        :param str title:
        """
        self.__name = name
        self.__titel = title

    def get_name(self):
        """
        Getter Methode fuer den Namen des Benutzers

        :return str: name
        """
        return self.__name

    def get_title(self):
        """
        Getter Methode fuer den Titel des Benutzers

        :return str: title
        """
        return self.__titel

    def set_title(self, title):
        """
        Setzt den Titel des Users
        :param str title:
        :return:
        """
        self.__titel = title

    def is_noble(self):
        """
        Gibt zurueck, ob der User adelig ist
        Falls titel noch nicht bestimmt (None) wird False returned
        :return boolean: adelig
        """

        if self.__titel is not None:
            if self.__titel == 'Fussvolk':
                return False
            else:
                return True
        else:
            return False
