#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : song.py
# @Author : Heimann, Jannik
# @Date   : 3/15/22
# @Desc   : Klasse, welche fuer das abspeichern von Lieder verwendet wird

class Song:
    # Konstruktor
    def __init__(self, bandname, titelname, genre, song_id=None, song_votes=None):
        self.id = song_id
        self.bandname = bandname
        self.titelname = titelname
        self.genre = genre
        self.votes = song_votes

    # Gibt den Song als String zurueck
    def toString(self):
        return str(self.bandname) + ' - ' + str(self.titelname) + ' - ' + str(self.genre)

    def get_bandname(self):
        return self.bandname

    def get_titelname(self):
        return self.titelname

    def get_genre(self):
        return self.genre

    # Methode, welche die Anzahl der Votes zurueck gibt
    def get_votes(self):
        return self.votes

    # Methode, welche die Song ID zurueck gibt
    def get_id(self):
        return self.id
