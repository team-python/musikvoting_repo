#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : jukebox.py.py
# @Author : Billerbeck, Ulf
# @Date   : 3/18/22
# @Desc   : Main Methode
from controller import Controller
from view import View

if __name__=="__main__":
    controller = Controller()
    view = View(controller)
    controller.set_view(view)
    view.start_log_in_window()