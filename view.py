#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : view.py
# @Author : Heimann, Jannik
# @Date   : 3/15/22
# @Desc   : GUI für Musikplaylist Programm

# Imports
from tkinter import *

from typing import List

from controller_interface import ControllerInterface as Controller
from view_interface import ViewInterface
from song import *

# Ausgabe String
user_txt = 'Bitte geben Sie Ihren Benutzername ein. ' + \
           '\nAnschließend können Sie Songs zu der Playlist hinzufügen \nund bis zu 5 Lieder upvoten.'


class View(ViewInterface):

    __cntrl: Controller
    __songlist: List[Song]
    __curr_opened_window: Tk
    __str_votes: StringVar
    __curr_sort: str

    def __init__(self, controller):
        """
        Konstruktor der Klasse
        :param Controller controller:
        """
        self.__cntrl = controller
        self.__songlist = controller.get_list()
        self.__curr_opened_window = None
        self.__str_votes = None
        self.__curr_sort = 'Name'

    def start_log_in_window(self):
        """
        Startet das Login Fenster
        :return:
        """

        # Schliesse altes Fenster
        self.close_curr_window()

        # Fenster
        login_window = Tk(className='partyplaylist - login')
        login_window.resizable(False, False)

        # Aktuelles Fenster zuweisen
        self.__curr_opened_window = login_window

        # Frames
        f_info_txt = Frame(login_window)
        f_info_txt.grid(column=0, row=0)
        f_input = Frame(login_window)
        f_input.grid(column=0, row=1)
        f_btn = Frame(login_window)
        f_btn.grid(column=0, row=2)

        # GUI Objekte
        # Info Text
        l_info_txt = Label(f_info_txt, text=user_txt, justify=LEFT)
        l_info_txt.grid(column=0, row=0)

        # Benutzernamen Eingabe
        l_benutzername = Label(f_input, text='Benutzername: ')
        e_benutzername = Entry(f_input)
        l_benutzername.grid(column=0, row=0, padx=5, pady=5)
        e_benutzername.grid(column=1, row=0, padx=5, pady=5)

        # Passwort Eingabe
        l_passwort = Label(f_input, text='Passwort: ')
        e_passwort = Entry(f_input)
        l_passwort.grid(column=0, row=1, padx=5, pady=5)
        e_passwort.grid(column=1, row=1, padx=5, pady=5)

        # Buttons
        btn_anmelden = Button(f_btn, text='Anmelden', command=lambda: self.__cntrl.login(e_benutzername.get(), e_passwort.get()))
        btn_registrieren = Button(f_btn, text='Registrieren',
                                  command=lambda: self.__cntrl.create_user(e_benutzername.get()))
        btn_passwort_vergessen = Button(f_btn, text='Passwort zurücksetzten',
                                        command=lambda: self.__cntrl.start_passwort_reset(e_benutzername.get()))
        btn_anmelden.grid(column=0, row=0, padx=5, pady=5)
        btn_registrieren.grid(column=0, row=1, padx=5, pady=5)
        btn_passwort_vergessen.grid(column=1, row=1, padx=5, pady=5)

        login_window.mainloop()

    def start_voting_window(self, is_noble):
        """
        Startet das Votefenster
        :param boolean is_noble:
        :return:
        """

        # Schliesse altes Fenster
        self.close_curr_window()

        # Fenster
        voting_window = Tk(className='Partyplaylist - Voting')
        voting_window.resizable(False, False)

        # Aktuelles Fenster zuweisen
        self.__curr_opened_window = voting_window

        # Frames
        lf_voting = LabelFrame(voting_window, text='Musik voten')
        lf_voting.grid(column=0, row=0, pady=5)
        lf_add_music = LabelFrame(voting_window, text='Musik hinzufügen')
        lf_add_music.grid(column=0, row=1)

        # Unterframes
        f_voting_links = Frame(lf_voting)
        f_voting_links.grid(column=0, row=0)
        f_voting_rechts = Frame(lf_voting)
        f_voting_rechts.grid(column=1, row=0)

        # Variablen
        self.__str_votes = StringVar()
        self.__str_votes.set('Votes: 0')

        # Ressourcen
        img_upvote = PhotoImage(file="Ressources/Button_Vote_up_25x25.png")

        # GUI Objekte
        # Voting Frame Links
        lb_list_songs = Listbox(f_voting_links, width=40)
        lb_list_songs.bind("<<ListboxSelect>>", self.__update_vote_string)    # Callback fkt, like "command=" but
                                                                              # with event parameter
        lb_list_songs.grid(column=0, row=0)
        sb_list_songs_y = Scrollbar(f_voting_links, orient=VERTICAL, command=lb_list_songs.yview)
        sb_list_songs_y.grid(column=1, row=0, sticky='ns')
        sb_list_songs_x = Scrollbar(f_voting_links, orient=HORIZONTAL, command=lb_list_songs.xview)
        sb_list_songs_x.grid(column=0, row=1, sticky='we')

        # Hinzufuegen der Scrollbars zu Listbox
        lb_list_songs.config(yscrollcommand=sb_list_songs_y.set, xscrollcommand=sb_list_songs_x.set)

        self.fill_list(lb_list_songs, self.__curr_sort)       # Befuelle die Listbox

        # Voting Frame Rechts
        btn_sort_name = Button(f_voting_rechts, text='Sort by Name',
                               command=lambda: self.fill_list(lb_list_songs, 'Name'))
        btn_sort_name.grid(column=0, row=0, padx=5, pady=5)
        btn_sort_votes = Button(f_voting_rechts, text='Sort by Votes',
                                command=lambda: self.fill_list(lb_list_songs, 'Votes'))
        btn_sort_votes.grid(column=0, row=1, padx=5, pady=5)
        btn_liste = Button(f_voting_rechts, text='Liste Ausgeben', command=lambda: self.show_music_list())
        btn_liste.grid(column=0, row=2, padx=5, pady=5)
        l_votes = Label(f_voting_rechts, font=('', 14), anchor=W, textvariable=self.__str_votes)
        l_votes.grid(column=0, row=3, padx=5, pady=5)
        btn_log_out = Button(f_voting_rechts, text='Bestätige und Logout', command=lambda: self.__cntrl.logout())
        btn_log_out.grid(column=0, row=4, padx=5, pady=5)

        # Vote Button wird nur angezeigt, wenn der User Vote berechtigt ist
        if is_noble:
            btn_vote_up = Button(f_voting_rechts, image=img_upvote,
                                 command=lambda: self.send_selected_vote_to_cntrl(lb_list_songs))
            btn_vote_up.grid(column=1, row=3, padx=5, pady=5)

        # Frame Musik hinzufügen
        l_bandname = Label(lf_add_music, text='Bandname: ')
        l_bandname.grid(column=0, row=0, padx=5)
        e_bandname = Entry(lf_add_music)
        e_bandname.grid(column=1, row=0, pady=5)
        l_titelname = Label(lf_add_music, text='Titelname: ')
        l_titelname.grid(column=0, row=1, padx=5)
        e_titelname = Entry(lf_add_music)
        e_titelname.grid(column=1, row=1, pady=5)
        l_genre = Label(lf_add_music, text='Genre: ')
        l_genre.grid(column=0, row=2, padx=5)
        e_genre = Entry(lf_add_music)
        e_genre.grid(column=1, row=2, pady=5)
        btn_hinzufuegen = Button(lf_add_music, text='Hinzufügen',
                                 command=lambda: self.add_song_to_list(lb_list_songs,
                                                                       e_bandname.get(),
                                                                       e_titelname.get(),
                                                                       e_genre.get()))
        btn_hinzufuegen.grid(column=2, row=3, padx=5, pady=5)

        voting_window.mainloop()

    def start_registering_window(self, is_noble=False, has_password=False):
        """
        Startet das Fenster zum Eintragen des Adelstitels.
        Sollte nur gestartet werden, wenn der User, welcher sich einloggt noch keinen Adelstitel besitzt
        :return:
        """

        # Schliesse altes Fenster
        self.close_curr_window()

        # Fenster
        registering_window = Tk(className='Registrierungsprozess')
        registering_window.resizable(False, False)

        # Aktuelles Fenster zuweisen
        self.__curr_opened_window = registering_window

        # Frame
        f_text = Frame(registering_window)
        f_text.grid(column=0, row=0)
        f_input = Frame(registering_window)
        f_input.grid(column=0, row=1)
        f_btn = Frame(registering_window)
        f_btn.grid(column=0, row=2)

        # GUI Objects


        rows = 0

        l_adelstitel = Label(f_input, text="Adelstitel: ")
        e_adelstitel = Entry(f_input)
        l_passwort = Label(f_input, text="Passwort: ")
        e_passwort = Entry(f_input)
        l_frage = Label(f_input, text="Sicherheitsfrage: ")
        e_frage = Entry(f_input)
        l_antwort = Label(f_input, text="Antwort: ")
        e_antwort = Entry(f_input)

        if not is_noble:
            # Adelstitel
            beschreibung = "Bitte fuegen Sie als Hoheit noch Ihren Adelsstamm hinzu, \n als Ritter bitte 'Ser' eintragen."
            l_info_txt = Label(f_text, text=beschreibung)
            l_info_txt.grid(column=0, row=0)
            l_adelstitel.grid(column=0, row=rows, padx=5, pady=5)
            e_adelstitel.grid(column=1, row=rows, padx=5, pady=5)
            rows += 1

        if not has_password:
            # Passwort
            l_passwort.grid(column=0, row=rows, padx=5, pady=5)
            e_passwort.grid(column=1, row=rows, padx=5, pady=5)
            rows += 1

            # Sicherheitsfrage
            l_frage.grid(column=0, row=rows, padx=5, pady=5)
            e_frage.grid(column=1, row=rows, padx=5, pady=5)
            rows += 1

            # Sicherheitsfragenantwort
            l_antwort.grid(column=0, row=rows, padx=5, pady=5)
            e_antwort.grid(column=1, row=rows, padx=5, pady=5)

        btn_bestaetigen = Button(f_btn, text="Bestaetigen",
                                 command=lambda: self.__cntrl.add_attributes_to_user(
                                     e_adelstitel.get(),
                                     e_passwort.get(),
                                     e_frage.get(),
                                     e_antwort.get()))

        btn_bestaetigen.grid(column=2, row=2, padx=5, pady=5)

        registering_window.mainloop()

    def start_password_reset_window(self, security_question):
        """
        Erstellt die GUI zum Zuruecksetzen des Passworts.
        Der Nuter muss die Sicherheitsfrage beantworten und ein neues Passwort eingeben.
        Ruft nach abschluss eine entsprechende Methode im Controller aus

        :param str security_question: Sicherheitsfrage
        :return None:
        """
        # Schliesse altes Fenster
        self.close_curr_window()

        # Fenster
        password_reset_window = Tk(className='Passwort zuruecksetzen')
        password_reset_window.resizable(False, False)

        # Aktuelles Fenster zuweisen
        self.__curr_opened_window = password_reset_window

        # Frames
        f_info_txt = Frame(password_reset_window)
        f_info_txt.grid(column=0, row=0)
        f_input = Frame(password_reset_window)
        f_input.grid(column=0, row=1)
        f_btn = Frame(password_reset_window)
        f_btn.grid(column=0, row=2)

        # Info text
        beschreibung = "Bitte geben sie die korrekte Antowrt zu Ihrer Sicherheitsabfrage und ihr neues Passwort an."
        l_info_txt = Label(f_info_txt, text=beschreibung)
        l_info_txt.grid(column=0, row=0)

        # Sicherheitsabfrage
        l_sicherheitsabfrage = Label(f_input, text=security_question)
        l_sicherheitsabfrage.grid(column=0, row=0, columnspan=2)

        # Sicherheitsabfragenantwort
        l_antwort = Label(f_input, text="Antwort: ")
        e_antwort = Entry(f_input)
        l_antwort.grid(column=0, row=1, padx=5, pady=5)
        e_antwort.grid(column=1, row=1, padx=5, pady=5)

        # Passwort
        l_passwort = Label(f_input, text="Passwort: ")
        e_passwort = Entry(f_input)
        l_passwort.grid(column=0, row=2, padx=5, pady=5)
        e_passwort.grid(column=1, row=2, padx=5, pady=5)

        # Button

        btn_reset = Button(f_btn, text='Reset', command=lambda: self.__cntrl.change_password(
            e_antwort.get(),
            e_passwort.get()))

        password_reset_window.mainloop()

    def show_music_list(self):
        """
        Oeffnet das Fenster, in welchem die Playlist angezeigt wird
        :return:
        """

        # Schliesse altes Fenster
        self.close_curr_window()

        # Fenster
        music_list_window = Tk(className='partyplaylist')
        music_list_window.geometry('350x200')
        music_list_window.resizable(False, False)

        # Aktuelles Fenster zuweisen
        self.__curr_opened_window = music_list_window

        # Variablen
        cnt = 0     # Counter fuer das hinzufuegen der Song Liste

        # GUI Objekte
        # Listbox, welche die Lieder enthaelt
        lb_liste = Listbox(music_list_window, width=55)
        lb_liste.grid(column=0, row=0)

        # Scrollbar fuer Liste in Y-Richtung
        sb_liste_y = Scrollbar(music_list_window, orient=VERTICAL, command=lb_liste.yview())
        sb_liste_y.grid(column=1, row=0, sticky='ns')

        # Scrollbar fuer Liste in X-Richtung
        sb_liste_x = Scrollbar(music_list_window, orient=HORIZONTAL, command=lb_liste.xview())
        sb_liste_x.grid(column=0, row=1, sticky='we')

        # Scrollbars zur Listbox hinzufuegen
        lb_liste.config(yscrollcommand=sb_liste_y.set, xscrollcommand=sb_liste_x.set)

        # Listbox befuellen mit allen Lieder, welche mind. 1 Vote haben
        self.__cntrl.sort_votes()
        song_list = self.__cntrl.get_list()

        for song in song_list:
            if song.get_votes() > 0:
                lb_liste.insert(cnt, str(song.get_votes()) + ' - ' + song.toString())
                cnt = cnt + 1

        music_list_window.mainloop()

    def add_song_to_list(self, liste, bandname, titelname, genre):
        """
        Fuegt einen Song der Playlist hinzu
        :param Listbox liste:
        :param str bandname:
        :param str titelname:
        :param str genre:
        :return:
        """

        new_song = Song(bandname, titelname, genre)
        self.__cntrl.add_song(new_song)
        self.fill_list(liste, self.__curr_sort)
        return

    def fill_list(self, lb_list_songs, art):
        """
        Fragt die Liste der Lieder von Controller ab und wandelt Sie in ein String Array um
        :param Listbox lb_list_songs:
        :param String art:
        :return:
        """

        song_arr = self.__songlist

        # Sortiere Liste anhand von Button auswahl
        if art == 'Name':
            self.__cntrl.sort_alphabetical()
            self.__curr_sort = art
        elif art == 'Votes':
            self.__cntrl.sort_votes()
            self.__curr_sort = art
        else:
            self.__curr_sort = ''
            print('Fehler, konnte Liste nicht Sortieren, Parameter art = ' + str(art) +
                  'konnte nicht interpretiert werden')

        # Listbox Inhalte loeschen
        lb_list_songs.delete(0, END)

        # Listbox neu befuellen
        for i in range(len(song_arr)):
            curr_song = song_arr[i].toString()
            lb_list_songs.insert(i, curr_song)

        return

    def close_curr_window(self):
        """
        Schliessen des aktuell geoeffneten Fensters.
        :return:
        """

        if self.__curr_opened_window is not None:
            self.__curr_opened_window.quit()
            self.__curr_opened_window.destroy()

        return

    def send_selected_vote_to_cntrl(self, listbox):
        """
        Diese Methode schickt das Objekt des Songs zu der Controller Funkiton add_vote
        Achtung! Das Attribute "votes" des Songs wurde noch nicht inkrementiert.
        :param Listbox listbox:
        :return:
        """

        for index in listbox.curselection():
            self.__cntrl.add_vote(self.__songlist[index])
            self.__str_votes.set("Votes: " + str(self.__songlist[index].get_votes()))

        return

    def __update_vote_string(self, event):
        """
        Methode updatet anhand des ausgewaehlten Songs die Vote anzeige auf der GUI.
        Wird ausschliesslich durch einen Callback der Listbox aufgerufen
        :param Event event:
        :return:
        """

        listbox = event.widget
        index = int(listbox.curselection()[0])
        self.__str_votes.set('Votes: ' + str(self.__songlist[index].get_votes()))
        return
