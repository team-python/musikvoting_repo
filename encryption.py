from hashlib import sha3_256
import os


def hash(input):
    h = sha3_256(bytes(input))
    return h.digest()


def new_salt():
    return os.urandom(32)
