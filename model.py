class model:
    def __init__(self):
        """
        Konstruktor der Klasse
        """

        pass

    @staticmethod
    def select(columns, table, where=None):
        """
        Erstellt die Anweisung die benoetigt wird, um die Daten welche als Parameter uebergeben werden
        aus der Tabelle abzufragen
        :param str[] columns:
        :param str table: Tabelle aus welcher Daten ausgelesen werden sollen
        :param str where: SQL WHERE Anweisung
        :returnstr :
        """

        s = f"SELECT {', '.join(columns)} FROM {table}"
        if where is not None:
            s = s + f" {where}"
        s = s + ";"
        return s

    @staticmethod
    def insert(table, columns, values):
        """
        Erstellt die Anweisung, welche fuer das einfuegen der uebergebenen Daten an die Datenbank gesendet werden muss
        :param str table: Tabelle in welche Daten hinzugefuegt werden sollen
        :param str[] columns:
        :param str[] values:
        :return str:
        """
        s = f"INSERT INTO {table} ({', '.join(columns)}) VALUES ("
        for v in values:
            if type(v) is int:
                s = s + str(v) + ", "
            else:
                s = s + f"'{str(v)}', "
        q = s[:-2] + ");"
        return q

    @staticmethod
    def update(table, columns, values, where=None):
        """
        Erstellt die Anweisung welche benötigt wird um die Daten welche als Parameter uebergeben werden
        in der SQL Datenbank zu verändern
        :param str table: Tabelle in welcher Daten veraendert werden sollen
        :param str[] columns:
        :param str[] values: Neue Werte, welche iengefuegt werden sollen
        :param str where: SQL WHERE Anweisung
        :return str:
        """

        cv_list = []
        for i in range(len(columns)):
            cv_list.append(str(columns[i]) + "=")
            if type(values[i]) is int:
                cv_list[i] += str(values[i])
            else:
                cv_list[i] += f"'{str(values[i])}'"

        s = f"UPDATE {table} SET {', '.join(cv_list)}"
        if where is not None:
            s = s + f" {where}"
        s = s +";"
        return s

    @staticmethod
    def select_musikwuensche_alph():
        """
        Gibt die Anweisung zurueck, welche die Datenbank die Songs anhand des Alphabets sortieren laesst
        :return:
        """

        return model.select(["p_song_id, titel, band, genre, count(p_vote_id) AS Votes"],
                            "t_musikwuensche tm "
                            "LEFT JOIN (SELECT p_vote_id, f_song_id FROM t_votes tv "
                                        "LEFT JOIN t_gaeste tg on tg.p_benutzername=tv.f_benutzername "
                                        "WHERE tg.adelstitel != 'Fussvolk') tv "
                            "on tm.p_song_id = tv.f_song_id",
                            "GROUP BY tm.p_song_id ORDER BY Band ASC, Titel ASC")

    @staticmethod
    def select_musikwuensche_votes():
        """
        Gibt die Anweisung zurueck, welche die Datenbank die Songs anhand der Votes sortieren laesst
        :return:
        """

        return model.select(["p_song_id, titel, band, genre, count(p_vote_id) AS Votes"],
                            "t_musikwuensche tm "
                            "LEFT JOIN (SELECT p_vote_id, f_song_id FROM t_votes tv "
                                        "LEFT JOIN t_gaeste tg on tg.p_benutzername=tv.f_benutzername "
                                        "WHERE tg.adelstitel != 'Fussvolk') tv "
                            "on tm.p_song_id = tv.f_song_id",
                            "GROUP BY tm.p_song_id ORDER BY Votes DESC")

    @staticmethod
    def vote(musikwunsch_id, benutzername):
        """
        Gibt die Anweisung zurueck, welche einen Song einen Vote hinzufuegt
        Achtung!: DB Trigger verhindert falsche Eingaben
        :param int musikwunsch_id:
        :param str benutzername:
        :return str:
        """

        return model.insert("t_votes", ["f_song_id", "f_benutzername"], [musikwunsch_id, benutzername])

    @staticmethod
    def deleteFirstVote(benutzername):
        """
        Gibt die Anweisung zurueck, um den am weitesten zurueckliegenden Vote des Nutzers zu loeschen
        :param str benutzername:
        :return str:
        """

        return f"DELETE FROM t_votes WHERE p_vote_id = (SELECT MIN(p_vote_id) FROM (SELECT * FROM t_votes) AS tv WHERE f_benutzername = '{benutzername}')"

    @staticmethod
    def countVotesByUser(benutzername):
        """
        Gibt die Anweisung zurueck, um die Anzahl der getaetigten Votes abzufragen
        :param str benutzername:
        :return str:
        """

        return model.select(["COUNT(p_vote_id)"], "t_votes", f"WHERE f_benutzername = '{benutzername}'")

    @staticmethod
    def new_song(titel, band, genre):
        """
        Gibt die Anweisung zurueck, um einen Song der Datenbank hinzuzufuegen
        :param str titel:
        :param str band:
        :param str genre:
        :return str:
        """
        return model.insert("t_musikwuensche", ["titel", "band", "genre"], [titel, band, genre])

    @staticmethod
    def new_user(benutzername):
        # TODO: Password und Benutzername hinzufuegen?
        """
        Gibt die Anweisung zurueck, um einen neuen Benutzer der Datenbank hinzuzufuegen
        :param str benutzername:
        :return:
        """
        return model.insert("t_gaeste", ["p_benutzername"], [benutzername])

    @staticmethod
    def login_check(name):
        """
        Gibt die Anweisung zurueck, welche von der Datenbank den Benutzernamen und den Adelstitel abfragt.
        :param str name: Name des Users
        :return str:
        """

        return model.select(["p_benutzername", "adelstitel"], "t_gaeste", f"WHERE p_benutzername = '{name}'")

    @staticmethod
    def update_title(benutzername, adelstitel):
        """
        Gibt die Anweisung zurueck, um in der Datenbank den Adelstitel des Benutzers zu veraendern
        :param str benutzername:
        :param str adelstitel: neuer Adelstitel
        :return str:
        """
        return model.update("t_gaeste", ["adelstitel"], [adelstitel], f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def set_security_question(benutzername, question):
        """
        Gibt die Anweisung zurueck, um bei einen Nutzer die Sicherheitsfrage zu aendern
        :param str benutzername:
        :param str question: Sicherheitsfrage, Maximal 256 Zeichen!
        :return str:
        """

        model.update("t_gaeste", ["question"], [question], f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def get_security_question(benutzername):
        """
        Gibt die Anweisung zurueck, welche von einem Nutzer die Sicherheitsfrage aus der Datenbank abfragt
        :param str benutzername:
        :return str:
        """

        return model.select(["question"], "t_gaeste", f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def set_password_hash(benutzername, password_hash):
        """
        Gibt die Anweisung zurueck, welche zu einem Benutzer das Passwort als Hash in der Datenbank abspeichert
        :param str benutzername:
        :param str password_hash: Passwort, welches bereits durch eine Hashfunktion veraendert wurde
        :return:
        """

        return model.update("t_gaeste", ["password"], [password_hash], f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def get_password_hash(benutzername):
        """
        Gibt die Anweisung zurueck, womit man den Passwort Hash eines Benutzers aus der Datenbank abfragt
        :param str benutzername:
        :return str:
        """

        return model.select(["password"], "t_gaeste", f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def set_answer_hash(benutzername, answer_hash):
        """
        Gibt die Anweisung zurueck, welche zu einem Benutzer die Antwort auf die Sicherheitsfrage
        als Hash in der Datenbank abspeichert
        :param str benutzername:
        :param str answer_hash: Antwort, welche bereits durch eine Hashfunktion veraendert wurde
        :return:
        """

        return model.update("t_gaeste", ["answer"], [answer_hash], f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def get_answer_hash(benutzername):
        """
        Gibt die Anweisung zurueck, womit man den Antwort Hash eines Benutzers aus der Datenbank abfragt
        :param str benutzername:
        :return str:
        """

        return model.select(["answer"], "t_gaeste", f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def set_salt(benutzername, salt):
        """
        Gibt die Anweisung zurueck, welche zu einem Benutzer den Salt in der Datenbank hinzufuegt
        :param str benutzername:
        :param str salt:
        :return:
        """

        return model.update("t_gaeste", ["salt"], [salt], f"WHERE p_benutzername = '{benutzername}'")

    @staticmethod
    def get_salt(benutzername):
        """
        Gibt die Anweisung zurueck, womit man den Salt eines Benutzers aus der Datenbank abfragt
        :param str benutzername:
        :return str:
        """

        return model.select(["salt"], "t_gaeste", f"WHERE p_benutzername = '{benutzername}'")
