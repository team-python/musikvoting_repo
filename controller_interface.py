#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : controller_interface.py
# @Author : Billerbeck, Ulf
# @Date   : 3/14/22
# @Desc   : Schnittstelle fuer die GUI


class ControllerInterface:
    """
    Interface, welches der Controller fuer GUI zugriffe implementiert.
    """

    def create_user(self, name):
        """
        Merkt sich den Username

        :param str name: Nutzername
        :return None:
        """
        pass

    def login(self, name, password):
        """
        Versucht einen Nutzer einzuloggen.
        Gibt eine Exception zurueck, wenn der Nutzer nicht vorhanden ist.
        Gibt eine Exception zurueck, wenn das Passwort falsch ist.

        :param str name: Nutzername
        :param password: Passwort
        :return None:
        """
        pass

    def password_reset_clicked(self, user):
        """
        Oeffnet neues Fenster zum Passwort Reset mithilfe der Sicherheitsfrage aus der DB

        :param user: username
        :return:
        """

    def get_security_question(self, username):
        """
        Ruft die Sicherheitsfrage des Benutzers ab.
        :param str username: benutzername
        :return str: Sicherheitsfrage
        """

    def change_password(self, security_question_answer, new_password):
        """
        Mit der richtigen Sicherheitsfrage wird das Passwort geaendert.

        :param security_question_answer: Antwort der Sicherheitsfrage
        :param new_password: Neues Passwort
        :return:
        """
    def logout(self):
        """
        Logt den aktuellen Nutzer aus.

        :return None:
        """
        pass

    def current_user(self):
        """
        Gibt den aktuellen Nutzer zurueck.

        :return User: Benutzer classenobjekt oder None, wenn kein Nutzer eingeloged ist.
        """
        pass

    def add_song(self, song):
        """
        Fuegt ein Lied in die Liederliste ein, wenn ein Benutzer angemeldet ist.
        Gibt eine Exception zurueck, falls kein Nutzer angemeldet ist.
        Gibt eine Exception zurueck, wenn das Lied bereits vorhanden ist.

        :param str song: Einzufuegendes Lied
        :return None:
        """
        pass

    def add_vote(self, song):
        """
        Fuegt dem Nutzer eine Stimme für den entsprechenden Song hinzu. Bis zu fünf Stimmen
        koennen abgegeben werden.
        Gibt eine Exception zurueck, falls kein Nutzer angemeldet ist.

        :param str song:
        :return None:
        """
        pass

    def get_list(self):
        """
        Gibt eine Liste von Tuplen (Lied, Stimmen) zurueck

        :return [Song]: Liederliste
        """
        pass

    def sort_alphabetical(self):
        """
        Sortiert die Liste alphabetisch

        :return None:
        """
        pass

    def sort_votes(self):
        """
        Sortiert die Liste nach der Anzahl der Votes.

        :return None:
        """
        pass

    def add_attributes_to_user(self, title, password, security_question, security_question_answer):
        """
        Erstellt neuen Gast in der Datenbank bzw. fuegt bestehendem Gast die Attribute hinzu.

        :param str title: Adelstitel
        :param str password: Passwort
        :param str security_question: Sicherheitsfrage
        :param str security_question_answer: Antwort Sicherheitsfrage
        :return:
        """
        pass

