#!/usr/bin/env python
#
# @license: Copyright (C) 2022 Continental AG and subsidiaries
# @File   : dbconnection.py
# @Author : Billerbeck, Ulf
# @Date   : 2/4/22
# @Desc   : A connection class to a Database via pymysql

import pymysql
import logging


class DBConnection:
    """
    Class that encapsulates a connection to a database via pymysql, offering various methods alongside.
    """
    _port = None
    _user = None
    _database = None
    _db_connection = None
    _cursor = None

    def __init__(self, host, port, user, password, database):
        """
        Constructor for the database class

        :param str host: Address which hosts the database
        :param int port: Port on which the database can be accessed
        :param str user: Database user
        :param str password: Password for the database user
        :param str database: Name of the database to access
        """
        self._host = host
        self._port = port
        self._user = user
        self._database = database
        self._db_connection, self._cursor = self._establish_db_connection(host, port, user, password, database)

    @staticmethod
    def _establish_db_connection(host, port, user, password, database):
        """
        Internal method to get a database connection via pymysql

        :param str host: Address which hosts the database
        :param int port: Port on which the database can be accessed
        :param str user: Database user
        :param str password: Password for the database userr
        :param str database: Name of the database to access
        :return Connection, Cursor: db_connection, cursor
        """
        con = pymysql.connect(host=host, port=port, user=user, password=password, db=database)
        cur = con.cursor()
        return con, cur

    def db_insert(self, query):
        """
        Inserts a list of key - value pairs into a table in the database

        :param str query: Table to insert into

        :return None:
        """
        try:
            self._cursor.execute(query)
            self._db_connection.commit()
        except Exception as e:
            self._db_connection.rollback()
            logging.info('Unable to execute query: ' + query + '\n')
            logging.info(e)
            raise e

    def db_select(self, query):

        result = None
        try:
            self._cursor.execute(query)
            result = self._cursor.fetchall()
        except Exception as e:
            logging.info('Unable to execute query: ' + query + '\n')
            logging.info(e)

        return result

    def get_host(self):
        """
        Getter method for the host of the database

        :return str: host
        """
        return self._host

    def get_port(self):
        """
        Getter method for the port the database is connected to

        :return int: port
        """
        return self._port

    def get_user(self):
        """
        Getter method for the user logged in to the database

        :return str: user
        """
        return self._user

    def get_database(self):
        """
        Getter method for the name of the database

        :return str: database
        """
        return self._database

    def get_db_connection(self):
        """
        Getter method for the  pymysql Connection object of a database

        :return Connection: connection
        """
        return self._db_connection

    def get_db_cursor(self):
        """
        Getter method for the cursor of a database connection

        :return cursor: cursor
        """
        return self._cursor
