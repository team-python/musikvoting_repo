CREATE DATABASE IF NOT EXISTS Jukebox;

CREATE USER IF NOT EXISTS JukeboxAdmin@'%' IDENTIFIED BY 'DatenbankLF9!';
GRANT delete, insert, select, update, alter ON Jukebox.* TO JukeboxAdmin@'%';

CREATE TABLE IF NOT EXISTS Jukebox.t_gaeste(
    p_benutzername VARCHAR(255) NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Jukebox.t_musikwuensche(
    p_song_id INT NOT NULL AUTO_INCREMENT
        PRIMARY KEY,
    titel VARCHAR(255) NOT NULL,
    band VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL,
    CONSTRAINT t_musikwuensche_titel_band_genre_uindex
        UNIQUE (titel, band, genre)
);

CREATE TABLE IF NOT EXISTS Jukebox.t_votes(
    p_vote_id INT NOT NULL AUTO_INCREMENT
        PRIMARY KEY,
    f_song_id INT NOT NULL,
    f_benutzername VARCHAR(255) NOT NULL,
    FOREIGN KEY (f_song_id) REFERENCES t_musikwuensche(p_song_id),
    FOREIGN KEY (f_benutzername) REFERENCES t_gaeste(p_benutzername)
);
